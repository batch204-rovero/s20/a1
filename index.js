let number = prompt("Enter a number");

for (number; number > 0; number--) {
	if (number % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.")
	} else if (number % 5 === 0) {
		console.log(number);
	}

	if (number < 50) {
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}
}

let string = "supercalifragilisticexpialidocious"

let consonants = "";

for (i = 0; i < string.length; i++) {
	if (string[i] === "a" ||
		string[i] === "e" ||
		string[i] === "i" ||
		string[i] === "o" ||
		string[i] === "u"
		) {
		console.log(string[i]);
	} else {
		consonants = consonants.concat(string[i]);
		console.log(consonants);
	}
}

